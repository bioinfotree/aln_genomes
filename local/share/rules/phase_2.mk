# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# alignments with nucmer

# TODO:
	

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

# log dir
logs:
	mkdir -p $@


reference.fasta.gz:
	ln -sf $(REFERENCE) $@


contigs.fasta:
	ln -sf $(CONTIGS) $@

reference.fasta: reference.fasta.gz
	zcat <$< >$@

# nucmer does an alignment of closely related nucleotide sequences
assembly.delta: reference.fasta contigs.fasta
	$(call load_modules); \
	nucmer \
	--mum \
	--prefix=$(basename $@) \
	$< \
	$^2


# sommare la colonna LEN2(F) e confrontare con il totale assemblato e il genoma di riferimento,
# percentuale d'identita' media e contiguita' (contig spezzati?)
assembly.tiles: assembly.delta
	$(call load_modules); \
	show-tiling \
	-i 95 \   * Set minimum percent identity to tile *
	-v 90 \   * Set minimum contig coverage to tile *
	-V 0 \   * Set minimum contig coverage difference *
	-a \   * Describe the tiling path by printing the tab-delimited alignment region coordinates to stdout *
	-g -1 \   * Set maximum gap between clustered alignments. A value of -1 will represent infinity *
	-u unused.contigs.tab \   * Output the tabdelimited alignment region coordinates of the unusable contigs to 'file' *
	$< >$@


unused.contigs.tab: assembly.tiles
	touch $@


.PHONY: test
test:
	@echo $(FASTA_BAMS)


ALL +=  reference.fasta.gz \
	contigs.fasta \
	assembly.delta \
	assembly.tiles



INTERMEDIATE += reference.fasta

CLEAN += unused.contigs.tab
