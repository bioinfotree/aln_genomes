# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# kmers frequency in regions covered by contigs Vs regions uncovered

# TODO:

REFERENCE ?=

CONTIGS ?=

SCAFFOLDS ?=

extern ../phase_1/assembly.bed as ASSEMBLY_BED
extern ../phase_1/assembly.bai as ASSEMBLY_BAI

log:
	mkdir -p $@

reference.fasta: reference.fasta.gz
	zcat <$(REFERENCE) >$@

covered.bed:
	ln -sf $(ASSEMBLY_BED) $@


assembly.bam: covered.bed chrs.size
	$(call load_modules); \
	bedToBam -i $< \
	-g $^2 \
	| samtools view -h - \   * bedToBam returns this error: terminate called after throwing an instance of 'std::runtime_error'  what():  BgzfStream::Write() - trying to write to non-writable IO device *
	| samtools view -bS - >$@   * by piping its output into samtools it runs *

assembly.bam: covered.bed reference.fasta
	$(call load_modules); \
	bedToBam -i $< \
	-g <(fasta_length <$^2) \
	| samtools view -h - \   * bedToBam returns this error: terminate called after throwing an instance of 'std::runtime_error'  what():  BgzfStream::Write() - trying to write to non-writable IO device *
	| samtools view -bS - >$@   * by piping its output into samtools it runs *

coverage.wig: log reference.fasta assembly.bam
	$(call load_modules); \
	compute_profile \
	--input-file $^3 \
	--sam-format \
	--fasta $^2 \
	--output-file $@ \
	2>&1 \
	| tee $</compute_profile.$@.log

coverage.hist: coverage.wig
	paste -s -d "\n" \
	<(coverage_WIG.pl --WIG $<) \
	<(histogram.pl --wig $<) \
	>$@

# print histogram of coverage. Use ylim="0:genome_covered"
# genome_covered can be found in coverage.hist
histogram.coverage.pdf: coverage.wig
	cov_plot.R \
	--title="coverage" \
	--ylim="0:500" \
	--xlim="0:50" \
	--max-cov=20 \
	--output-file=$@ \
	<$<

# tranform to compressed bigWig
coverage.bw: coverage.wig reference.fasta
	$(call load_modules); \
	wigToBigWig $< <(fasta_length <$^2) $@




.PHONY: test
test:
	@echo $(FASTA_BAMS)


ALL += 

INTERMEDIATE += 

CLEAN += 