#!/usr/bin/perl
###########################################
# Description: takes a wig wiggle file    #
#              and returns coverage stats #
###########################################

use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use Switch;

my $wig;
my $max = 10000;
my $help = 0;

GetOptions(
	'wig=s'  => \$wig,
	'help|?'       => \$help
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(1) if (!defined($wig));

my @histogram;
for (my $i=0; $i<= $max; $i++) {
	$histogram[$i] = 0;
}

open(WIG, $wig) or die("Could not open input $wig file\n");
while (my $line = <WIG>) {
	if ($line !~ /fixed/) {
		chomp $line;
		if ($line < $max) {
			$histogram[$line]++;
		}
		else {
			$histogram[$max]++;
		}
	}
}
close WIG;

my $mean = 0;
my $freq = 0;
for (my $i=0; $i<= $max; $i++) {
	$mean += $i * $histogram[$i];
	$freq += $histogram[$i];
}
my $mean_all = int(($mean / $freq) * 100) / 100;
print "Mean\t".$mean_all."\n";

my $partial_sum = 0;
for (my $i=0; $i<= $max; $i++) {
	$partial_sum += $histogram[$i];
	if ($partial_sum > $freq / 2) {
		print "Median\t".$i."\n";
		last;
	}
}

my $max_value = 0;
my $mode = -1;
for (my $i=1; $i<= $max; $i++) {
	$mean += $i * $histogram[$i];
	$freq += $histogram[$i];
	if ($histogram[$i] > $max_value) {
		$max_value = $histogram[$i];
		$mode = $i;
	}
}
if ($histogram[0] > $max_value) {
	print "Mode\t0\n";
}
else {
	print "Mode\t".$mode."\n";
}

$freq -= $histogram[0];
$mean = int(($mean / $freq) * 100) / 100;
print "Mean (no zero)\t".$mean."\n";

$partial_sum = 0;
for (my $i=1; $i<= $max; $i++) {
	$partial_sum += $histogram[$i];
	if ($partial_sum > $freq / 2) {
		print "Median (no zero)\t".$i."\n";
		last;
	}
}

print "Mode (no zero)\t".$mode."\n";

# print frequencies
for (my $i=0; $i<= $max; $i++) {
	if ($histogram[$i] > 0) {
		print $i . "\t" . $histogram[$i] . "\n";
	}
}



__END__


=head1 GFF Statistics

histogram.pl - Using this script

=head1 SYNOPSIS

perl histogram.pl [--wig <filename>] [--help]


  Options:
    --wig <filename>		input wiggle file
    --help			print this help message

output on standard output (redirect, if necessary)
