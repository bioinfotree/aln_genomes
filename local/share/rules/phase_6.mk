# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# calculate assemblathon statistics

# TODO:

extern ../phase_1/assembly.bam as EXTERN_ASSEMBLY

context prj/check_hemizygosity

# reference
REFERENCE ?=
# contigs
CONTIGS ?=
# scaffolds
SCAFFOLDS ?=

MIN_LEN := 1000

N_NUM := 1


log:
	mkdir -p $@

contigs.fasta:
	ln -sf $(CONTIGS) $@

scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

scaffolds.bed:
	scaffolds2contigs <$(FINAL_SUMMARY) >$@

reference.fasta:
	zcat <$(REFERENCE) >$@

ref.len: reference.fasta
	echo "$$(fasta_count -l $<)" >$@

# # problems in alignments with last can arise from the use of [multiple] or ungapped alignment
# ifeq ($(EXTERN_ALN),yes)
# compass.stats: reference.fasta contigs.fasta ref.len
	# $(call load_modules); \
	# compass.pl -s samtools -l $(MIN_LEN) -n $$(cat $^3) -x 1000 -y 1000 -a $(EXTERN_ASSEMBLY) -- $< $^2 >$@
# else
# compass.stats: reference.fasta contigs.fasta ref.len
	# $(call load_modules); \
	# compass.pl -s samtools -l $(MIN_LEN) -n $$(cat $^3) -x 1000 -y 1000 -- $< $^2 >$@
# endif

# compassRun.TEMP.contigsVSref.bam.bai: compass.stats
	# $(call load_modules); \
	# samtools index $(basename $@)

scaffolds.stats: ref.len contigs.fasta scaffolds.bed scaffolds.fasta
	$(call load_modules); \
	export PERL5LIB=$$PERL5LIB:$$PRJ_ROOT/local/bin; \
	assemblathon_stats.pl -csv -graph -n $(N_NUM) -genome_size $$(cut -f1 $<) -unscaff -contig $^2 -bed $^3 $^4 \
	| sed "s/[-]\{2,\}//g"   \   * remove repetition of 2 or more - *
	| sed "s/^[ \t]*//" \   * remove first blank chr in front of lines *
	| sed "s/[ ]\{2\}/\t/" \   * transform double spaces into tab *
	| sed '/^$$/{1,1d;}' >$@   * remove first line if empty *

.META: scaffolds.unscaff
	1	scaffold
	2	contig

scaffolds.scaffold.LG50.csv scaffolds.contig.LG50.csv scaffolds.unscaff: scaffolds.stats
	touch $@


.META: scaffolds.%.LG50.tab
	1	X
	2	LGX length

scaffolds.%.LG50.tab: scaffolds.%.LG50.csv
	tr ',' \\t <$< \
	| cut --complement -f 1 \
	| transpose \
	| bawk '!/^[\#+,$$]/ { if ( $$2 > 0 ) print $$0; }' >$@

scaffolds.%.LG50.pdf: scaffolds.%.LG50.tab
	scatterplot -t histogram -e -o $@ -t "$(SAMPLE_NAME) $* LGX" --xlab="X" --ylab="LGX length" -p 20 1,2 <$<

contigs.scaffolds.LG50.png: scaffolds.contig.LG50.pdf scaffolds.scaffold.LG50.pdf
	montage -density 150x150 -quality 100 -mode concatenate -tile 1x $^ $@


.PHONY: test
test:
	@echo 


ALL += log \
	ref.len \
	scaffolds.bed \
	scaffolds.stats \
	scaffolds.contig.LG50.tab \
	scaffolds.scaffold.LG50.tab \
	contigs.scaffolds.LG50.png \
	scaffolds.unscaff

	# compassRun.TEMP.contigsVSref.bam.bai \
	# compass.stats \
	# contigs.fasta \

INTERMEDIATE += scaffolds.scaffold.LG50.csv \
	scaffolds.contig.LG50.csv \
	scaffolds.contig.LG50.pdf \
	scaffolds.scaffold.LG50.pdf

CLEAN += $(wildcard *.csv)

	# $(wildcard compassRun.*) \
	 # $(wildcard scaffolds.*) \
	 # compass.graph.png \
	 # reference.fasta.fai \
	 # rdotplot.R
