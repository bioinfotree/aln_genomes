# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# contigs and scaffolds length distribution

# TODO:
	

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

# log dir
logs:
	mkdir -p $@


reference.fasta.gz:
	ln -sf $(REFERENCE) $@


contigs.fasta:
	ln -sf $(CONTIGS) $@

scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

reference.fasta: reference.fasta.gz
	zcat <$< >$@

target.A.pdf: contigs.fasta
	fasta_length $< \
	| bsort -k 2,2n \
	| distrib_plot -t histogram -c 1 -o $@

target.B.pdf: scaffolds.fasta
	fasta_length $< \
	| bsort -k 2,2n \
	| distrib_plot -t histogram -c 1 -o $@

target.C.txt: contigs.fasta
	fasta_length $< \
	| bsort -k 2,2n \
	| stat_base -a -m -s -v -S -t 2 >$@

target.D.txt: scaffolds.fasta
	fasta_length $< \
	| bsort -k 2,2n \
	| stat_base -a -m -s -v -S -t 2 >$@


target.E.pdf: contigs.fasta scaffolds.fasta
	paste \
	<(fasta_length $< | cut -f2) \
	<(fasta_length $^2 | cut -f2) \
	| bsort -k 1,1g -k 2,2g \
	| distrib_plot -t histogram -n -o $@ 1 2

pn_rpv3_cfranc_contigs.pdf: $(CONTIGS_PN) contigs.fasta $(CONTIGS_CFRANC)
	paste \
	<(fasta_length $< | cut -f2) \
	<(fasta_length $^2 | cut -f2) \
	<(fasta_length $^3 | cut -f2) \
	| bsort -k 1,1g -k 2,2g \
	| distrib_plot -t histogram -n -o $@ 1 2 3

pn_rpv3_cfranc_scaffolds.pdf: $(SCAFFOLDS_PN) scaffolds.fasta $(SCAFFOLDS_CFRANC)
	paste \
	<(fasta_length $< | cut -f2) \
	<(fasta_length $^2 | cut -f2) \
	<(fasta_length $^3 | cut -f2) \
	| bsort -k 1,1g -k 2,2g \
	| distrib_plot -t histogram -n -o $@ 1 2 3


.PHONY: test
test:
	@echo $(FASTA_BAMS)


ALL +=  reference.fasta.gz \
	contigs.fasta \
	target.A.pdf \
	target.B.pdf \
	target.C.txt \
	target.D.txt



INTERMEDIATE += reference.fasta

CLEAN += 
