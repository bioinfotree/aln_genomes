# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# contig metrics Vs heterozygosity

# TODO:

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=


extern ../phase_1/assembly.bed as ASSEMBLY_BED

extern ../phase_1/assembly.bai as ASSEMBLY_BAI

extern ../phase_1/custom.het.bed as CUSTOM_HET_BED

extern ../phase_1/custom.homo.bed as CUSTOM_HOMO_BED


# log dir
logs:
	mkdir -p $@

reference.fasta.gz:
	ln -sf $(REFERENCE) $@

contigs.fasta:
	fasta2tab <$(CONTIGS) \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 >$@

scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

reference.fasta: reference.fasta.gz
	zcat <$< >$@

heterozygosity.seg:
	ln -sf $(HETEROZYGOSITY_SEG) $@

assembly.bed:
	ln -sf $(ASSEMBLY_BED) $@

custom.het.bed:
	ln -sf $(CUSTOM_HET_BED) $@

custom.homo.bed:
	ln -sf $(CUSTOM_HOMO_BED) $@

custom.%.seg: custom.%.bed
	bawk '!/^[\#+,$$]/ { print "custom_$*",$$1,$$2,$$3,$$7; }' $< >$@

.META: custom.%.contigs.bed
	1	chr	chr1
	2	start	405130
	3	stop	406723
	4	name of features in (a) that overlap	contig_9709
	5	score	250
	6	strand	+
	7	thickStart	405130
	8	thickEnd	406723
	9	itemRgb	0,0,0
	10	The number of blocks (exons) in the BED line	1
	11	length of block in (a) that map	1593,
	12	start of block (a) that map	0,
	13	original chr from (b)	chr1
	14	original start from (b)	400000
	15	original end from (b)	500000

chrs.size: reference.fasta.gz
	zcat $< | fasta_length >$@

# coordinates that represents portion of the genomes not covered by custom files
custom.complement.bed: custom.homo.seg custom.het.seg chrs.size heterozygosity.seg
	$(call load_modules); \
	bedtools complement \
	-i <(cat $< $^2 | cut -f 2,3,4 | sortBed -i stdin) \
	-g $^3 \
	| bedtools intersect \
	-bed \   * write output as BED, not bam *
	-loj \
	-a stdin \
	-b <(bawk '!/^[\#+,$$,ID]/ { print $$2,$$3,$$4,$$5; }' $^4) \
	| mergeBed \
	-i stdin \
	-d -1 \
	-c 7 \
	-o "min,max,mean,median" >$@




# get number of contigs that overlap a given interval
custom.%.scaffolds.bed: assembly.bed custom.%.bed
	$(call load_modules); \
	bedtools intersect \
	-bed \   * write output as BED, not bam *
	-wb \   * write the original entry in (b) for each overlap *
	-wa \   * write the original entry in (a) for each overlap *
	-a $< \
	-b $^2 \
	>$@

#	-f 0.51 \   * minimum overlap of intervals (b) on contigs (a). 51% so one contigs can lie on only one interval *

# It is possible that the same sequence (scaffold or contig) appear in both homozygous and heterozygous fractions
# In this rules I assign the sequences that appears in both fractions to the fraction in witch it align for grater length
# For example, the list of sequences that have to be leaved to the homozygous fraction must be removed from the heterozygous fraction.
# For this reason they are put into the filter.from.het.
filter.from.het: custom.homo.scaffolds.bed custom.het.scaffolds.bed
	select_columns 4 11 <$< \   * select scaffold name and length of scaffold that map *
	| tr -d ',' \
	| bsort --key=1,1 \   * sort by chrs *
	| collapsesets -s -o 2 \   * collapse lengths of blacks of scaffolds that appears multiple times *
	| bawk '!/^[\#+,$$]/ { \
	split($$2,a,";"); \
	TOT_LEN=0; \
	for (i in a) { \
		TOT_LEN += a[i]; \   * sum the lengths of collapsed blocks *
		} \
	print $$1,TOT_LEN; \
	}' \
	| translate -k -a \   * draw near, lengths of blocks of scaffolds that map in both first and second prerequisites *
	<(select_columns 4 11 <$^2 \   * select scaffold name and length of scaffold that map *
	| tr -d ',' \
	| bsort --key=1,1 \   * sort by chrs *
	| collapsesets -s -o 2 \   * collapse lengths of blacks of scaffolds that appears multiple times *
	| bawk '!/^[\#+,$$]/ { \
	split($$2,a,";"); \
	TOT_LEN=0; \
	for (i in a) { \
		TOT_LEN += a[i]; \   * sum the lengths of collapsed blocks *
		} \
	print $$1,TOT_LEN; \
	}') 1 \
	| bawk '!/^[\#+,$$]/ { \
	if ( $$2 >= $$3 ) { print $$1>"filter.from.homo"; } \   * if a block map for more length in the second prerequisite, it go in the target file *
	else { print $$1; } \
	}' >$@

# file generated by rules in filter.from.het
filter.from.homo: filter.from.het
	touch $@

# from homozygous fraction, remove scaffolds that map in heterozygous fraction for a greater length, and vice-versa
custom.no.commons.%.scaffolds.bed: custom.%.scaffolds.bed filter.from.%
	filter_1col -v 4 $^2 <$< >$@


# A scaffold can map in one of the two homozygous and heterozygous fraction and also in the complement fraction
# (fraction of the genome that are not classified as homozygous or heterozygous).
# in this rule I list scaffolds that map for a greater length in the complement fraction with respetct to the homozygous or
# heterozygous fractions.
# For example, if a scaffold that map in the homozygous fraction, also map for the most of its length to the complement fraction,
# it will be listed into the cause.in.complement.filter.from.homo. This list will be used later to remove it
# from the custom.homo.scaffolds.bed file.
cause.in.complement.filter.from.%: custom.no.commons.%.scaffolds.bed custom.complement.scaffolds.bed
	cat $< \
	| select_columns 4 11 \   * select scaffold name and length of scaffold that map *
	| bsort --key=1,1 \   * sort by chrs *
	| collapsesets -s -o 2 \   * collapse lengths of blacks of scaffolds that appears multiple times *
	| bawk '!/^[\#+,$$]/ { \
	split($$2,a,";"); \
	TOT_LEN=0; \
	for (i in a) { \
		TOT_LEN += a[i]; \   * sum the lengths of collapsed blocks *
		} \
	print $$1,TOT_LEN; \
	}' \
	| translate -k -a <(select_columns 4 11 <$^2) 1 \   * draw near, lengths of blocks of scaffolds that map in both first and second prerequisites *
	| bawk '!/^[\#+,$$]/ { \
	if ( $$2 >= $$3 ) { print $$1; } \   * if a block map for more length in the second prerequisite, it go in the target file *
	}' >$@

# from homozygous fraction, remove scaffolds that map in complement fraction for a greater length.
# Do the same for heterozygous fraction
custom.no.complement.%.scaffolds.bed: custom.no.commons.%.scaffolds.bed cause.in.complement.filter.from.%
	filter_1col -v 4 $^2 <$< >$@

# append contigs length to contigs name
custom.complement.scaffolds.len: scaffolds.fasta custom.complement.scaffolds.bed
	translate -a -k <(fasta_length <$<) 4 <$^2 >$@

# append contigs length to contigs name
custom.%.scaffolds.len: scaffolds.fasta custom.no.complement.%.scaffolds.bed
	translate -a -k <(fasta_length <$<) 4 <$^2 >$@



.META: custom.%.scaffolds.len.tmp
	1	length_of_the_scaffold	2738
	2	name_of_the_scaffold	scaffold_2913
# Produce a TSV file with: scaffold length, scaffold mapped length, scaffold_name
# contig names may appear multiple times in different intervals.
# since metrics are calculated along all intervals here I made contigs unique.
custom.%.scaffolds.len.tmp: custom.%.scaffolds.len
	select_columns 5 12 4 <$< \
	| bsort -r -g -k 1,1 -k 3,3 \
	| uniq -f2 >$@


# check common scaffolds
common.scaffolds: custom.no.complement.homo.scaffolds.bed custom.no.complement.het.scaffolds.bed
	comm -1 -2 \
	<(cut -f4 $< | bsort) \
	<(cut -f4 $^2 | bsort) \
	| bsort \
	| uniq \
	| wc -l >$@


.META: custom.%.scaffolds.metrics
	1	N50	10343
	2	L50	57869
	3	N50L	196916650
	4	total_length	162853431
	5	total_mapped_length	162853431
	6	scaffold_number	34087
	7	N90	10343
	8	L90	57869
	9	N90L	196916650
	
# calcolate N50, N50L, L50 for all contigs
 
# 1. Read Fasta file and calculate sequence length.
# 2. Sort length on reverse order.
# 3. Calculate Total size.
# 4. Calculate N50.
# 
# ## Read Fasta File and compute length ###
# my $length;
# my $totalLength;
# my @arr;
# while(){
#    chomp;
#    if(/>/){
#    push (@arr, $length);
#    $totalLength += $length;
#    $length=0;
#    next;
#   }
#   $length += length($_);
# }
# 
# close(FH);
# 
# my @sort = sort {$b <=> $a} @arr;
# my $n50;
# my $L50;
# foreach my $val(@sort){
#      $n50+=$val;
#      $L50++;
#       if($n50 >= $totalLength/2){
#          print "N50 length is $n50 and N50 value is: $val and L50 is $L50\n";
#          last;
#      }
# }
custom.%.scaffolds.metrics: custom.%.scaffolds.len.tmp
	paste \
	<(bawk  -v TOT_LEN="$$(stat_base --precision=100000 --total 1 <$<)" \
		-v TOT_MAPPED_LEN="$$(stat_base --precision=100000 --total 2 <$<)" \
		-v NROWS="$$(cat $< | wc -l)" \
	'BEGIN { N50L=0; N50=0; }  !/^[\#+,$$]/ { \
	N50L += $$1; \
	N50++; \
	if ( N50L >= (TOT_LEN/2) ) { printf "%i\t%i\t%i\t",N50,$$1,N50L; exit 0; } \
	} END { printf "%i\t%i\t%i\n",TOT_LEN,TOT_MAPPED_LEN,NROWS; }' <$<) \
	<(bawk -v TOT_LEN="$$(stat_base --precision=100000 --total 1 <$<)" -v NROWS="$$(cat $< | wc -l)" 'BEGIN { N90L=0; N90=0; }  !/^[\#+,$$]/ { \
	N90L += $$1; \
	N90++; \
	if ( N90L >= (TOT_LEN*0.9) ) {  exit 0; } \
	} END { printf "%i\t%i\t%i\n",N90,$$1,N90L; }' <$<) \
	>$@


# # for testing purpose
# target.B: target.A.tmp
# 	bawk -v TOT_LEN="$$(stat_base --precision=100000 --total 2 <$<)" -v NROWS="$$(cat $< | wc -l)" 'BEGIN { N50L=0; N50=0; }  !/^[\#+,$$]/ { \
# 	N50L += $$2; \
# 	N50 += 1; \
# 	if ( N50L >= (TOT_LEN/2) ) { printf "%i\t%i\t%i\t",N50,$$2,N50L; exit 0; } \
# 	} END { printf "%i\t%i\n",TOT_LEN,NROWS; }' <$< >$@;



.META: N50.%.het.intervals.metrics.seg
	1	feature name	N50
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	19632

# calcolate N50, N50L, L50 for contigs in each interval
N50.custom.%.intervals.metrics.seg: custom.%.scaffolds.len
	bawk \
	-v norm_N50="False" \
	'BEGIN \
	{ \   * initialize empty variables *
		i=0; \
		start=""; \
		delete a; \   * empty vector *
		print "#track type=\"Other\" name=\"N50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"$@"; \
		print "#track type=\"Other\" name=\"N50L_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"N50L.$(@:N50.%=%)"; \
		print "#track type=\"Other\" name=\"L50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"L50.$(@:N50.%=%)"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"$@"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"N50L.$(@:N50.%=%)"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"L50.$(@:N50.%=%)"; \
	} \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \   * for the first line only save the values *
	{ \
		start=$$15; \   * save segment end *
		a[$$4]=$$5; \   * save contig length using contig name as index so duplicated contigs are saved only one time *
		#print $$0; \
		printf "N50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
		printf "N50L_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"N50L.$(@:N50.%=%)"; \
		printf "L50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"L50.$(@:N50.%=%)"; \
	} \
	else \
	{ \
		if ( $$15 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			N50L=0; \
			TOT_LEN=0; \
			for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
			n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
			#printf "contig lengths:\t%i\n",n; \
			#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
			#printf "\n"; \
			for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
			{ \
				N50L += b[i]; \   * save current contig length *
				N50 = (n-i+1); \   * save reverse index (last position becomes first) *
				if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
				{ \
					#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
					if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
					printf "%i\n",N50L >"N50L.$(@:N50.%=%)"; \
					printf "%i\n",b[i] >"L50.$(@:N50.%=%)"; \
					break; \
				} \
			} \
			delete a; \   * reset array *
			start = $$15; \   * update start with new segment end *
			i=0; \   * reset array index *
			printf "N50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
			printf "N50L_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"N50L.$(@:N50.%=%)"; \
			printf "L50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"L50.$(@:N50.%=%)"; \
		} \
		a[$$4]=$$5; \   * add to array the current contig length *
		#print $$0; \
	} \
	i++; \   * update index *
	} \
	END { \   * print data of last interval *
		N50L=0; \
		TOT_LEN=0; \
		for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
		n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
		#printf "contig lengths:\t%i\n",n; \
		#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
		#printf "\n"; \
		for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
		{ \
			N50L += b[i]; \   * save current contig length *
			N50 = (n-i+1); \   * save reverse index (last position becomes first); N50 nomalized for number of contigs *
			if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
			{ \
				#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
				if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
				printf "%i\n",N50L >"N50L.$(@:N50.%=%)"; \
				printf "%i\n",b[i] >"L50.$(@:N50.%=%)"; \
				break; \
			} \
		} \
		delete a; \   * reset array *
	}' $< >$@


normN50.custom.%.intervals.metrics.seg: custom.%.scaffolds.len
	bawk \
	-v norm_N50="True" \
	'BEGIN \
	{ \   * initialize empty variables *
		i=0; \
		start=""; \
		delete a; \   * empty vector *
		print "#track type=\"Other\" name=\"normN50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"$@"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"$@"; \
	} \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \   * for the first line only save the values *
	{ \
		start=$$15; \   * save segment end *
		a[$$4]=$$5; \   * save contig length using contig name as index so duplicated contigs are saved only one time *
		#print $$0; \
		printf "normN50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
	} \
	else \
	{ \
		if ( $$15 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			N50L=0; \
			TOT_LEN=0; \
			for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
			n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
			#printf "contig lengths:\t%i\n",n; \
			#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
			#printf "\n"; \
			for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
			{ \
				N50L += b[i]; \   * save current contig length *
				N50 = (n-i+1); \   * save reverse index (last position becomes first) *
				if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
				{ \
					#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
					if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
					break; \
				} \
			} \
			delete a; \   * reset array *
			start = $$15; \   * update start with new segment end *
			i=0; \   * reset array index *
			printf "normN50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
		} \
		a[$$4]=$$5; \   * add to array the current contig length *
		#print $$0; \
	} \
	i++; \   * update index *
	} \
	END { \   * print data of last interval *
		N50L=0; \
		TOT_LEN=0; \
		for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
		n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
		#printf "contig lengths:\t%i\n",n; \
		#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
		#printf "\n"; \
		for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
		{ \
			N50L += b[i]; \   * save current contig length *
			N50 = (n-i+1); \   * save reverse index (last position becomes first); N50 nomalized for number of contigs *
			if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
			{ \
				#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
				if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
				break; \
			} \
		} \
		delete a; \   * reset array *
	}' $< >$@



.META: N50L.custom.%.intervals.metrics.seg
	1	feature name	N50L
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	568338

N50L.custom.%.intervals.metrics.seg: N50.custom.%.intervals.metrics.seg
	touch $@


.META: L50.custom.%.intervals.metrics.seg
	1	feature name	L50
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	39

L50.custom.%.intervals.metrics.seg: N50.custom.%.intervals.metrics.seg
	touch $@


scatterplot.custom.all.Vs.%.pdf: custom.homo.seg %.custom.homo.intervals.metrics.seg custom.het.seg %.custom.het.intervals.metrics.seg custom.complement.bed %.custom.complement.intervals.metrics.seg
	paste \
	<(bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^2 \
	| translate -a -f 2 <(bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $<) 2 \
	| select_columns 4 5) \
	<(bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^4 \
	| translate -a -f 2 <(bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^3) 2 \
	| select_columns 4 5) \
	<(bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^6 \
	| translate -a -f 1 <(bawk '!/^[\#+,$$,ID]/ { print $$1":"$$2":"$$3,$$4,$$5; }' $^5) 2 \
	| select_columns 4 5) \
	| bawk '!/^[\#+,$$]/ { print $$1,$$2,$$3,$$4,$$5,$$6; }' \
	| scatterplot \
	--remove-na \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs contigs $*" \
	--xlab="heterozygosity" \
	--ylab="$*" \
	--pch="1,2,3" \
	--output-file=$@ \
	--xlim="0:0.008" \
	--color="blue,red,orange" \
	1,2 3,4 5,6

boxplot.custom.all.%.pdf: %.custom.het.intervals.metrics.seg %.custom.homo.intervals.metrics.seg %.custom.complement.intervals.metrics.seg
	paste <(tail -n +3 $< \
	| cut -f5 \
	| sed '1i heterozygous') \
	<(tail -n +3 $^2 \
	| cut -f5 | sed '1i homozygous') \
	<(tail -n +3 $^3 \
	| cut -f5 | sed '1i unclassified') \
	| boxplot -n -o $@


.PHONY: test
test:
	@echo


ALL +=  custom.het.bed  \
	custom.homo.bed \
	custom.complement.bed \
	reference.fasta.gz \
	contigs.fasta \
	scaffolds.fasta \
	heterozygosity.seg \
	custom.homo.seg \
	custom.het.seg \
	assembly.bed \
	custom.homo.scaffolds.bed \
	custom.het.scaffolds.bed \
	custom.complement.scaffolds.bed \
	filter.from.het \
	filter.from.homo \
	custom.no.commons.het.scaffolds.bed \
	custom.no.commons.homo.scaffolds.bed \
	cause.in.complement.filter.from.het \
	cause.in.complement.filter.from.homo \
	custom.no.complement.het.scaffolds.bed \
	custom.no.complement.homo.scaffolds.bed \
	custom.het.scaffolds.len \
	custom.homo.scaffolds.len \
	custom.complement.scaffolds.len \
	common.scaffolds \
	custom.het.scaffolds.metrics \
	custom.homo.scaffolds.metrics \
	custom.complement.scaffolds.metrics \
	N50.custom.het.intervals.metrics.seg \
	N50.custom.homo.intervals.metrics.seg \
	N50.custom.complement.intervals.metrics.seg \
	normN50.custom.het.intervals.metrics.seg \
	normN50.custom.homo.intervals.metrics.seg \
	normN50.custom.complement.intervals.metrics.seg \
	N50L.custom.het.intervals.metrics.seg \
	N50L.custom.homo.intervals.metrics.seg \
	N50L.custom.complement.intervals.metrics.seg \
	scatterplot.custom.all.Vs.N50.pdf \
	scatterplot.custom.all.Vs.L50.pdf \
	scatterplot.custom.all.Vs.N50L.pdf \
	scatterplot.custom.all.Vs.normN50.pdf \
	boxplot.custom.all.N50.pdf \
	boxplot.custom.all.L50.pdf \
	boxplot.custom.all.N50L.pdf \
	boxplot.custom.all.normN50.pdf




INTERMEDIATE += reference.fasta \
		custom.het.scaffolds.len.tmp \
		custom.homo.scaffolds.len.tmp \
		custom.complement.scaffolds.len.tmp \
		chrs.size


CLEAN += 
