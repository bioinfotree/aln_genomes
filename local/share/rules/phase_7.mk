# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# calculate N50 L50 N50L of contigs in windows with hight and low kmer frequencies

# TODO:

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=


extern ../phase_1/assembly.bed as ASSEMBLY_BED
# extern ../../rpv3/phase_1/assembly.bam as ASSEMBLY_BAM
extern ../phase_1/assembly.bai as ASSEMBLY_BAI
#extern ../../rpv3/phase_1/assembly.bam.bai  as ASSEMBLY_BAI
extern ../phase_4/kmer.freq.bw as KMER_FREQ_BW

# log dir
logs:
	mkdir -p $@


reference.fasta.gz:
	ln -sf $(REFERENCE) $@


kmer.freq.bw:
	ln -sf $(KMER_FREQ_BW) $@


assembly.bed:
	ln -sf $(ASSEMBLY_BED) $@

chrs.size: reference.fasta.gz
	zcat $< | fasta_length >$@

assembly.bam: assembly.bed chrs.size
	$(call load_modules); \
	bedToBam -i $< \
	-g $^2 \
	| samtools view -h - \
	| samtools view -bS - >$@

contigs.fasta:
	fasta2tab <$(CONTIGS) \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 >$@


scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@


reference.fasta.fai: reference.fasta.gz
	$(call load_modules); \
	samtools faidx $< && mv $<.fai $@


# make windows along the genome
regions.bed: reference.fasta.fai
	$(call load_modules); \
	bedtools makewindows -g $< -i srcwinnum -w 100000 >$@

# calculate median of kmers distribution indside windows
kmer.freq.bed: kmer.freq.bw regions.bed
	$(call load_modules); \
	bigWigAverageOverBed -bedOut=$@ $< $^2 $(basename $@).tab


# divide between windows of high and low repetition level
# set kmer frequency greater then the homozigous peak from the kmer-spectrum of PN40024
# the analysis should be performed on all chromosomes not the QUERY_NAME
high.kmer.freq.bed: kmer.freq.bed
	bawk '!/^[#+,$$,ID]/ { \
	if ( $$5 > 200 ) { print $$0>"$@"; } \
	else if ( $$5 >= 100 && $$5 <= 200 ) { print $$0 >"intermediate.kmer.freq.bed"; } \
	else { print $$0 >"low.kmer.freq.bed"; } \
	}' $<


low.kmer.freq.bed: high.kmer.freq.bed
	touch $@

intermediate.kmer.freq.bed: high.kmer.freq.bed
	touch $@

# kmers in regions covered by contigs an regions not covered in bed file

# # tranform alignments to bed12 format
# covered.bed: assembly.bam
# 	$(call load_modules); \
# 	bamToBed \
# 	-color "255,0,0" \
# 	-cigar \
# 	-split \
# 	-bed12 \
# 	-i $< \
# 	>$@


.META: %.kmer.contigs.bed
	1	chr	chr1
	2	start	405130
	3	stop	406723
	4	name of features in (a) that overlap	contig_9709
	5	score	250
	6	strand	+
	7	thickStart	405130
	8	thickEnd	406723
	9	itemRgb	0,0,0
	10	The number of blocks (exons) in the BED line	1
	11	length of block in (a) that map	1593,
	12	start of block (a) that map	0,
	13	original chr from (b)	chr1
	14	original start from (b)	400000
	15	original end from (b)	500000


# get number of contigs that overlap a given interval
%.kmer.contigs.bed: assembly.bam %.kmer.freq.bed
	$(call load_modules); \
	bedtools intersect \
	-f 0.51 \   * minimum overlap of intervals (b) on contigs (a). 51% so one contigs can lie on only one interval *
	-bed \   * write output as BED, not bam *
	-wb \   * write the original entry in (b) for each overlap *
	-wa \   * write the original entry in (a) for each overlap *
	-a $< \
	-b $^2 >$@


# append contigs length to contigs name
%.kmer.contigs.len: scaffolds.fasta %.kmer.contigs.bed
	translate -a -k <(fasta_length <$<) 4 <$^2 >$@

# contig names may appear multiple times in different intervals.
# since metrics are calculated along all intervals here I made contigs unique
%.kmer.contigs.tmp: %.kmer.contigs.len
	select_columns 5 4 <$< \
	| bsort -r -g -k 1,1 -k 2,2 \
	| uniq -f1 >$@


.META: %.kmer.contigs.metrics
	1	N50	10343
	2	L50	57869
	3	N50L	196916650
	4	total length	162853431
	5	contigs number	34087

# see phase_5 for explanation of the algorithm
%.kmer.contigs.metrics: %.kmer.contigs.tmp
	bawk -v TOT_LEN="$$(stat_base  --total 1 <$<)" -v NROWS="$$(cat $< | wc -l)" 'BEGIN { N50L=0; N50=0; }  !/^[\#+,$$]/ { \
	N50L += $$1; \
	N50++; \
	if ( N50L >= (TOT_LEN/2) ) { printf "%i\t%i\t%i\t",N50,$$1,N50L; exit 0; } \
	} END { printf "%i\t%i\n",TOT_LEN,NROWS; }' <$< >$@;



.META: N50.%.kmer.intervals.metrics.seg
	1	feature name	N50
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	19632

# calcolate N50, N50L, L50 for contigs in each interval
N50.%.kmer.intervals.metrics.seg: %.kmer.contigs.len
	bsort -g -k 14,16 $< \   * sort by segments by chr and coordinates in ascending order *
	| bawk \
	-v norm_N50="False" \
	'BEGIN \
	{ \   * initialize empty variables *
		i=0; \
		start=""; \
		delete a; \   * empty vector *
		print "#track type=\"Other\" name=\"N50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"$@"; \
		print "#track type=\"Other\" name=\"N50L_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"N50L.$(@:N50.%=%)"; \
		print "#track type=\"Other\" name=\"L50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"L50.$(@:N50.%=%)"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"$@"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"N50L.$(@:N50.%=%)"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"L50.$(@:N50.%=%)"; \
	} \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \   * for the first line only save the values *
	{ \
		start=$$15; \   * save segment end *
		a[$$4]=$$5; \   * save contig length using contig name as index so duplicated contigs are saved only one time *
		#print $$0; \
		printf "N50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
		printf "N50L_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"N50L.$(@:N50.%=%)"; \
		printf "L50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"L50.$(@:N50.%=%)"; \
	} \
	else \
	{ \
		if ( $$15 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			N50L=0; \
			TOT_LEN=0; \
			for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
			n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
			#printf "contig lengths:\t%i\n",n; \
			#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
			#printf "\n"; \
			for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
			{ \
				N50L += b[i]; \   * save current contig length *
				N50 = (n-i+1); \   * save reverse index (last position becomes first) *
				if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
				{ \
					#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
					if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
					printf "%i\n",N50L >"N50L.$(@:N50.%=%)"; \
					printf "%i\n",b[i] >"L50.$(@:N50.%=%)"; \
					break; \
				} \
			} \
			delete a; \   * reset array *
			start = $$15; \   * update start with new segment end *
			i=0; \   * reset array index *
			printf "N50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
			printf "N50L_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"N50L.$(@:N50.%=%)"; \
			printf "L50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"L50.$(@:N50.%=%)"; \
		} \
		a[$$4]=$$5; \   * add to array the current contig length *
		#print $$0; \
	} \
	i++; \   * update index *
	} \
	END { \   * print data of last interval *
		N50L=0; \
		TOT_LEN=0; \
		for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
		n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
		#printf "contig lengths:\t%i\n",n; \
		#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
		#printf "\n"; \
		for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
		{ \
			N50L += b[i]; \   * save current contig length *
			N50 = (n-i+1); \   * save reverse index (last position becomes first); N50 nomalized for number of contigs *
			if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
			{ \
				#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
				if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
				printf "%i\n",N50L >"N50L.$(@:N50.%=%)"; \
				printf "%i\n",b[i] >"L50.$(@:N50.%=%)"; \
				break; \
			} \
		} \
		delete a; \   * reset array *
	}' >$@


normN50.%.kmer.intervals.metrics.seg: %.kmer.contigs.len
	bsort -g -k 14,16 $< \   * sort by segments by chr and coordinates in ascending order *
	| bawk \
	-v norm_N50="True" \
	'BEGIN \
	{ \   * initialize empty variables *
		i=0; \
		start=""; \
		delete a; \   * empty vector *
		print "#track type=\"Other\" name=\"normN50_$*\" color=\"50,150,255\" graphType=\"bar\" scaleType=\"linear\"" >"$@"; \
		print "ID\tchrom\tloc.start\tloc.end\tseg.mean" >"$@"; \
	} \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \   * for the first line only save the values *
	{ \
		start=$$15; \   * save segment end *
		a[$$4]=$$5; \   * save contig length using contig name as index so duplicated contigs are saved only one time *
		#print $$0; \
		printf "normN50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
	} \
	else \
	{ \
		if ( $$15 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			N50L=0; \
			TOT_LEN=0; \
			for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
			n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
			#printf "contig lengths:\t%i\n",n; \
			#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
			#printf "\n"; \
			for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
			{ \
				N50L += b[i]; \   * save current contig length *
				N50 = (n-i+1); \   * save reverse index (last position becomes first) *
				if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
				{ \
					#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
					if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
					break; \
				} \
			} \
			delete a; \   * reset array *
			start = $$15; \   * update start with new segment end *
			i=0; \   * reset array index *
			printf "normN50_$*\t%s\t%s\t%s\t",$$14,$$15,$$16 >"$@"; \
		} \
		a[$$4]=$$5; \   * add to array the current contig length *
		#print $$0; \
	} \
	i++; \   * update index *
	} \
	END { \   * print data of last interval *
		N50L=0; \
		TOT_LEN=0; \
		for (i in a) { TOT_LEN += a[i]; } \   * sum lengths of all contigs *
		n = asort(a, b); \   * sort vector values (contig lengths) in ascending order. Sorted copy in b *
		#printf "contig lengths:\t%i\n",n; \
		#for (i=n; i>=1; i--) { printf "%i\t",b[i]; } \
		#printf "\n"; \
		for (i=n; i>=1; i--) \   * iterate in reverse order i.e. descending order *
		{ \
			N50L += b[i]; \   * save current contig length *
			N50 = (n-i+1); \   * save reverse index (last position becomes first); N50 nomalized for number of contigs *
			if ( N50L >= (TOT_LEN/2) ) \   * test if N50 is reached *
			{ \
				#printf "metrics: %i\t%i\t%i\n",b[i],N50L,L50; \
				if ( norm_N50=="False" ) \
						{ printf "%.2f\n",N50 >"$@"; } \
					else { printf "%.2f\n",(N50/n) >"$@"; } \
				break; \
			} \
		} \
		delete a; \   * reset array *
	}' >$@



.META: N50L.%.kmer.intervals.metrics.seg
	1	feature name	N50L
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	568338

N50L.%.kmer.intervals.metrics.seg: N50.%.kmer.intervals.metrics.seg
	touch $@


.META: L50.%.kmer.intervals.metrics.seg
	1	feature name	L50
	2	chr	chr1
	3	start	405130
	4	stop	406723
	5	feature value	39

L50.%.kmer.intervals.metrics.seg: N50.%.kmer.intervals.metrics.seg
	touch $@

# N.B.: composing a single scatterplot for low, intermediate and high kmer freqs is not possible because columns have different number of rows

scatterplot.%.kmer.freq.Vs.N50.pdf: %.kmer.freq.bed N50.%.kmer.intervals.metrics.seg
	bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^2 \
	| translate -a <(bawk '!/^[\#+,$$,ID]/ { print $$1":"$$2":"$$3,$$5; }' $<) 2 \
	| scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) $* kmer frequency Vs contigs N50" \
	--xlab="kmer frequency" \
	--ylab="N50" \
	--output-file=$@ \
	3,4

scatterplot.%.kmer.freq.Vs.L50.pdf: %.kmer.freq.bed L50.%.kmer.intervals.metrics.seg
	bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^2 \
	| translate -a <(bawk '!/^[\#+,$$,ID]/ { print $$1":"$$2":"$$3,$$5; }' $<) 2 \
	| scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) $* kmer frequency Vs contigs L50" \
	--xlab="kmer frequency" \
	--ylab="L50" \
	--output-file=$@ \
	3,4


scatterplot.%.kmer.freq.Vs.N50L.pdf: %.kmer.freq.bed N50L.%.kmer.intervals.metrics.seg
	bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^2 \
	| translate -a <(bawk '!/^[\#+,$$,ID]/ { print $$1":"$$2":"$$3,$$5; }' $<) 2 \
	| scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) $* kmer frequency Vs contigs N50L" \
	--xlab="kmer frequency" \
	--ylab="N50L" \
	--output-file=$@ \
	3,4

scatterplot.%.kmer.freq.Vs.normN50.pdf: %.kmer.freq.bed normN50.%.kmer.intervals.metrics.seg
	bawk '!/^[\#+,$$,ID]/ { print $$1,$$2":"$$3":"$$4,$$5; }' $^2 \
	| translate -a <(bawk '!/^[\#+,$$,ID]/ { print $$1":"$$2":"$$3,$$5; }' $<) 2 \
	| scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) $* kmer frequency Vs contigs normN50" \
	--xlab="kmer frequency" \
	--ylab="norm50" \
	--output-file=$@ \
	3,4



.PHONY: test
test:
	@echo $(FASTA_BAMS)


ALL +=  reference.fasta.gz \
	kmer.freq.bw \
	assembly.bam \
	scaffolds.fasta \
	kmer.freq.bed \
	regions.bed \
	high.kmer.freq.bed \
	low.kmer.freq.bed \
	intermediate.kmer.freq.bed \
	high.kmer.contigs.bed \
	low.kmer.contigs.bed \
	intermediate.kmer.contigs.bed \
	high.kmer.contigs.metrics \
	low.kmer.contigs.metrics \
	intermediate.kmer.contigs.metrics \
	N50.high.kmer.intervals.metrics.seg \
	N50.low.kmer.intervals.metrics.seg \
	N50.intermediate.kmer.intervals.metrics.seg \
	normN50.high.kmer.intervals.metrics.seg \
	normN50.low.kmer.intervals.metrics.seg \
	normN50.intermediate.kmer.intervals.metrics.seg \
	N50L.high.kmer.intervals.metrics.seg \
	N50L.low.kmer.intervals.metrics.seg \
	N50L.intermediate.kmer.intervals.metrics.seg \
	L50.high.kmer.intervals.metrics.seg \
	L50.low.kmer.intervals.metrics.seg \
	L50.intermediate.kmer.intervals.metrics.seg \
	scatterplot.high.kmer.freq.Vs.N50.pdf \
	scatterplot.low.kmer.freq.Vs.N50.pdf \
	scatterplot.intermediate.kmer.freq.Vs.N50.pdf \
	scatterplot.high.kmer.freq.Vs.L50.pdf \
	scatterplot.low.kmer.freq.Vs.L50.pdf \
	scatterplot.intermediate.kmer.freq.Vs.L50.pdf \
	scatterplot.high.kmer.freq.Vs.N50L.pdf \
	scatterplot.low.kmer.freq.Vs.N50L.pdf \
	scatterplot.intermediate.kmer.freq.Vs.N50L.pdf \
	scatterplot.high.kmer.freq.Vs.normN50.pdf \
	scatterplot.low.kmer.freq.Vs.normN50.pdf \
	scatterplot.intermediate.kmer.freq.Vs.normN50.pdf


INTERMEDIATE += reference.fasta

CLEAN += 
