# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# calculate coverages of several genomic features

# TODO:

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

# coordinates of blocks from: fully placed scaffolds and scaffolds placed after splitting
extern ../phase_1/assembly.bed as ASSEMBLY_BED
# coordinates of blocks from fully placed scaffolds only
# extern ../phase_1/assembly.placed-full.bed as ASSEMBLY_BED
# coordinates corresponds to the alignments that anchor contigs to the reference. Only for placed scaffolds (not splitted)
#extern ../phase_1/assembly.aligns.bed as ASSEMBLY_BED


ELEMENTS := gene exon repeat lnc_antisense lnc_intergenic lnc_intronic

# log dir
logs:
	mkdir -p $@


reference.fasta.gz:
	ln -sf $(REFERENCE) $@

assembly.bed:
	ln -sf $(ASSEMBLY_BED) $@

reference.fasta.fai: reference.fasta.gz
	$(call load_modules); \
	samtools faidx $< && mv $<.fai $@

gene.gff3:
	ln -sf $(GENES) $@

# it is a gtf
repeats.gtf:
	ln -sf $(REPEATS) $@

lnc_antisense.gff3:
	ln -sf $(LNC_ANTISENSE) $@

lnc_intergenic.gff3:
	ln -sf $(LNC_INTERGENIC) $@

lnc_intronic.gff3:
	ln -sf $(LNC_INTRONIC) $@

chrs.size: reference.fasta.gz
	zcat $< \
	| fasta_length >$@


# genes.intersect.bed: genes.gff3 assembly.bam 
# 	$(call load_modules); \
# 	bedtools intersect \
# 	-bed \
# 	-wao \
# 	-a $< \
# 	-b $^2 >$@
# 
# genes.covered.bed: assembly.bam genes.gff3
# 	$(call load_modules); \
# 	bedtools coverage \
# 	-abam $< \
# 	-b $^2 \
# 	-hist \
# 	-d \
# 	>$@
# 
# genes.map.bed: genes.gff3 assembly.bam
# 	$(call load_modules); \
# 	sortBed -i $< \
# 	| bedtools map \
# 	-a stdin \
# 	-b $^2 \
# 	>$@

# translate to BED format
repeats.bed: repeats.gtf
	$(call load_modules); \
	gtf2bed <$< \
	| sed -e 's/Protein/Te/g' \
	| bawk '!/^[$$,\#]/ { \
	if ( $$3-$$2 > 1000 ) print $$0; \   * remove repeats shorter then 1000bp *
	}'>$@   * gtf format is also known as gff2 format *

gene.bed: gene.gff3 
	$(call load_modules); \
	gff2bed <$< >$@

lnc_%.bed: lnc_%.gff3
	$(call load_modules); \
	sed 's/;$$//g' <$< \   * lnc_ files has dangerous ";" last character that crash gff2bed. Remove it *
	| gff2bed >$@


MAPPED_BED = $(addsuffix .bed,$(addprefix mapped.,$(ELEMENTS)))

# map genes coordinates to aligned part of contigs. Contigs belong to
# scaffolds that was placed via "Golden Blocks" as defined by ScaPA tool
mapped.%.bed: %.bed assembly.bed
	$(call load_modules); \
	sort-bed $< \
	| bedmap \
	--echo \
	--fraction-ref 0.8 \
	--prec 3 \
	--delim "\t" \
	--count \
	--echo-overlap-size \
	--bases-uniq-f \
	--echo-map-id-uniq \
	- \   * reference file *
	$^2 \   * mapping file *
	>$@



# extract regions that are not covered
uncovered.bed: assembly.bed chrs.size
	$(call load_modules); \
	complementBed -i $< -g $^2 >$@


PRESENT_BED = $(addsuffix .bed,$(addprefix present.,$(ELEMENTS)))

# extract only placed elements that are genes
present.gene.bed: mapped.gene.bed
	bawk '!/^[$$,\#]/ { \
	  if ( $$8 ~ /gene/ ) \
	  { \
	   if ( $$11 == 0 ) \
	    { \
	      print $$0>"not$@"; \
	    } \
	   else \
	   { \
	     print $$0>"$@"; \
	   } \
	  } \
	}' $<

# extract only placed elements that are exons
present.exon.bed: mapped.gene.bed
	bawk '!/^[$$,\#]/ { \
	  if ( $$8 ~ /exon/ ) \
	  { \
	   if ( $$11 == 0 ) \
	    { \
	      print $$0>"not$@"; \
	    } \
	   else \
	   { \
	     print $$0>"$@"; \
	   } \
	  } \
	}' $<

# extract only placed elements that are genes
present.lnc_%.bed: mapped.lnc_%.bed
	bawk '!/^[$$,\#]/ { \
	  if ( $$8 ~ /gene/ ) \
	  { \
	   if ( $$11 == 0 ) \
	    { \
	      print $$0>"not$@"; \
	    } \
	   else \
	   { \
	     print $$0>"$@"; \
	   } \
	  } \
	}' $<

# extract every element. Each one is a repeat
present.repeat.bed: mapped.repeats.bed
	bawk '!/^[$$,\#]/ { \
	   if ( $$11 == 0 ) \
	    { \
	      print $$0>"not$@"; \
	    } \
	   else \
	   { \
	     print $$0>"$@"; \
	   } \
	}' $<


NOTPRESENT_BED = $(addsuffix .bed,$(addprefix notpresent.,$(ELEMENTS)))

notpresent.%.bed: present.%.bed
	touch $@


define PER_CHR_STATS
bawk 'BEGIN { i=0; start=""; count=0; aligned=0; item_length=0; } \
!/^[$$,\#]/ { \
if ( NR == 1 ) { \
	start = $$1; \
} \
else { \
	if ( $$1 != start ) { \
		{ print start, count, item_length, aligned; } \
		start = $$1; \
		count = 0; \
		aligned = 0; \
		item_length = 0; \
		i++; \
		} \
	} \
	count ++; \
	aligned += $$12; \
	item_length += $$3-$$2; \
} \
END { print start,count,item_length,aligned; }' $1 >$2
endef


# calculate statistics per chrs
present.%.stats: present.%.bed
	$(call PER_CHR_STATS,$<,$@)

# calculate statistics per chrs
notpresent.%.stats: notpresent.%.bed
	$(call PER_CHR_STATS,$<,$@)


# # test PER_CHR_STATS function
# %.out: %.test
# 	bawk 'BEGIN { aligned=0; item_length=0; } \
# 	!/^[$$,\#]/ { \
# 		aligned += $$12; \
# 		item_length += $$3-$$2; \
# 	} END { print $$1,NR,item_length,aligned; }' $< >$@


.META: present.notpresent.%.stats present.notpresent.stats.all
	1	chr
	2	count of present elements (#)
	3	count of present elements (%)
	4	total length of elements
	5	total length of aligned parts of elements
	6	count of NOT present elements (#)
	7	count of NOT present elements (%)
	8	total length of NOT present elements
	9	total length of aligned parts of NOT present elements
	10	TOT elements
	11	TOT length



PRESENT_NOTPRESENT_STATS = $(addsuffix .stats,$(addprefix present.notpresent.,$(ELEMENTS)))

# join present and notPresent data
# present.*.stats can contain data for certain chrs that are not present
# in notpresent.*.stats. So first of all I have to obtain the complete list
# of chrs in both files. Then I translate each one against the
# complete list
present.notpresent.%.stats: present.%.stats notpresent.%.stats
	cat $^ \
	| cut -f1 \
	| bsort \
	| uniq \   * obtain complete list of all chrs in both files *
	| translate -v -a $^2 1 \
	| translate -v -a $< 1 \
	| bawk 'BEGIN { PRESENT_ELEM = 0; \
	TOT_LEN_ELEM = 0; \
	TOT_LEN_ALN_ELEM = 0; \
	NOT_PRESENT_ELEM = 0; \
	TOT_LEN_NOT_PRESENT_ELEM = 0; \
	TOT_LEN_ALN_NOT_PRESENT_ELEM = 0; \
	STOT_ELEM = 0; \
	STOT_LEN = 0; } \
	!/^[$$,\#]/ { \
	TOT_elem = $$2 +$$5; \
	TOT_len = $$3 + $$6; \
	printf "%s\t%i\t%.4f\t%i\t%i\t%i\t%.4f\t%i\t%i\t%i\t%i\n", $$1,$$2, $$2/TOT_elem, $$3, $$4, $$5, $$5/TOT_elem, $$6, $$7, TOT_elem, TOT_len; \
	PRESENT_ELEM += $$2; \
	TOT_LEN_ELEM += $$3; \
	TOT_LEN_ALN_ELEM += $$4; \
	NOT_PRESENT_ELEM += $$5; \
	TOT_LEN_NOT_PRESENT_ELEM += $$6; \
	TOT_LEN_ALN_NOT_PRESENT_ELEM += $$7; \
	STOT_ELEM += TOT_elem; \
	STOT_LEN += TOT_len; } \
	END { printf "$*\t%i\t%.4f\t%i\t%i\t%i\t%.4f\t%i\t%i\t%i\t%i\n", PRESENT_ELEM, PRESENT_ELEM/STOT_ELEM, TOT_LEN_ELEM, TOT_LEN_ALN_ELEM, NOT_PRESENT_ELEM, NOT_PRESENT_ELEM/STOT_ELEM, TOT_LEN_NOT_PRESENT_ELEM, TOT_LEN_ALN_NOT_PRESENT_ELEM, STOT_ELEM, STOT_LEN; }' >$@


present.notpresent.stats.all: $(PRESENT_NOTPRESENT_STATS)
	>$@; \
	for FILE in $^; do \
		echo "$$FILE" | rev | cut -d "." -f  2 | rev >>$@; \
		bawk -M $$FILE | cut -f2 | transpose >>$@; \
		cat $$FILE >>$@; \
		echo -e "\n" >>$@; \
	done

.META: present.notpresent.stats.short
	1	category
	2	count of present elements (#)
	3	count of present elements (%)
	4	total length of elements
	5	total length of aligned parts of elements
	6	count of NOT present elements (#)
	7	count of NOT present elements (%)
	8	total length of NOT present elements
	9	total length of aligned parts of NOT present elements
	10	TOT elements
	11	TOT length

present.notpresent.stats.short: $(PRESENT_NOTPRESENT_STATS)
	>$@; \
	for FILE in $^; do \
		tail -n 1 <$$FILE >>$@; \
	done


.PHONY: test
test:
	@echo $(MAPPED_BED)




ALL +=  reference.fasta.gz \
	assembly.bed \
	uncovered.bed \
	$(PRESENT_BED) \
	$(NOTPRESENT_BED) \
	$(PRESENT_NOTPRESENT_STATS) \
	present.notpresent.stats.all \
	present.notpresent.stats.short




INTERMEDIATE += reference.fasta

CLEAN += $(wildcard *.gff3) \
	 $(wildcard *.gtf) \
	 $(wildcard *.bed) \
	 chrs.size
